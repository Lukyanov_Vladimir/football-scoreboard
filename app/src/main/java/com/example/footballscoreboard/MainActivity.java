package com.example.footballscoreboard;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Integer counter1 = 0, counter2 = 0;

    TextView textViewCounter1, textViewCounter2;

    Button addBtn1, addBtn2, reset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewCounter1 = (TextView) findViewById(R.id.counter1);
        textViewCounter2 = (TextView) findViewById(R.id.counter2);

        addBtn1 = (Button) findViewById(R.id.addButton);
        addBtn2 = (Button) findViewById(R.id.addButton2);
        reset = (Button) findViewById(R.id.resetButton);

        addBtn1.setOnClickListener(this);
        addBtn2.setOnClickListener(this);
        reset.setOnClickListener(this);

        textViewCounter1.setText(counter1.toString());
        textViewCounter2.setText(counter2.toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("counter", counter1);
        outState.putInt("counter2", counter2);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        counter1 = savedInstanceState.getInt("counter");
        counter2 = savedInstanceState.getInt("counter2");

        textViewCounter1.setText(counter1.toString());
        textViewCounter2.setText(counter2.toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.addButton:
                counter1++;

                textViewCounter2.setText(counter1.toString());
                break;

            case R.id.addButton2:
                counter2++;

                textViewCounter1.setText(counter2.toString());
                break;

            case R.id.resetButton:
                counter1 = 0;
                counter2 = 0;

                textViewCounter1.setText(counter1.toString());
                textViewCounter2.setText(counter2.toString());
                break;
        }
    }
}
